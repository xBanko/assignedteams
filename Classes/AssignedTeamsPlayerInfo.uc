class AssignedTeamsPlayerInfo extends Actor;

var AssignedTeamsPlayerInfo Next;
var string Key;
var string ForcedNick;
var int Team;

// Methods
// Key is the Nickname they connected with
// ForcedNick will be their forced nickname
static function AssignedTeamsPlayerInfo Create(String key, int team, String forcedNick)
{
	local AssignedTeamsPlayerInfo Obj;
	Obj = new class'AssignedTeamsPlayerInfo';
    Obj.Key = key;
    Obj.Team = team;
    Obj.ForcedNick = forcedNick;
	return Obj;
}